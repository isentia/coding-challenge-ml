# Machine Learning Coding Challenge

## Problem Description

As a media monitoring company, we try to make sense of the media that is created every day. One area in particular is written articles, where we want to automatically determine the type of the article. This knowledge helps us to filter articles and present to our customers only what they really care about.

You are given a corpus of text articles, included in this repo. We have split the corpus into test.txt.gzip and train.txt.gzip. This split was made arbitrarily, there is no structural difference between both and you may choose to ignore it if you wish.

Your job is to create a service that classifies the articles into related groups by detecting patterns inside the dataset. When your service is given a new article, your model(s) should return the type(s) that this article belongs to.

The problem is unsupervised. The corpus contains a label field for each article but it is unreliable and you can ignore it.

This could be a text classification problem, or a clustering problem, or a topic detection problem or a combination of these. It is up to you to define. There are no right or wrong answers so long as your approach is properly justified. The important thing is that you demonstrate your ability to translate a vague (but realistic) problem into a machine learning model that gives a reasonable solution, and that you are able to explain the choices that you make in creating a model and solving this task.


## Technical specifications

Your program should be written in Python 3. You can use any other library of your choice that you are familiar with.

You must check in your code in a git repository. You do not have to check in the corpus again, unless you made changes to it.

You can use the full corpus or part of it, if you want to save on computation time. You will not be judged on the accuracy of your final model. What matters is your justification for the approach that you take.

You may use a notebook to explain/solve problems, make graphs or show tables.

## Extra questions

Answers to the following questions should be given in the accompanying readme

- How do you evaluate the accuracy and correctness of your model(s)?
- What could you do to improve the accuracy of your model?
- How would you serve your model(s) in production?
- Imagine that there are future requirements that ask you to add a new class/topic/pattern? What would re-training look like? Are there any concerns? How would you avoid these concerns?


## Evaluation

Your solution will be judged on the following criteria:

- inclusion of a readme with explanation and answers
- completeness of the solution
- clear overall design
- readability of the code
- clear instructions on how to run your solution
- use of git

Bonus

- demo code for serving your model(s).

## Submission

Host your repository online (e.g. on github), make sure that it is public so that we can see it, and tell us the URL.

Make sure your submission includes a readme where you explain your reasoning, the decisions you made, and your answers to the extra questions. Without this documentation we cannot evaluate your code!
